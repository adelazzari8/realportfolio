const mysql = require('mysql');


// Créez une connexion MySQL en spécifiant les informations de la base de données
export default connection =  mysql.createConnection({
  host: import.meta.env.VITE_LIEN_BDD,     // Adresse du serveur MySQL
  user: import.meta.env.VITE_USER_BDD, // Nom d'utilisateur MySQL
  password: import.meta.env.VITE_PASSWORD_BDD, // Mot de passe MySQL
  database: import.meta.env.VITE_NAME_BDD,// Nom de la base de données MySQL
  port: 3306
});

// Établissez la connexion à la base de données
connection.connect((err) => {
  if (err) {
    console.error('Erreur de connexion à MySQL : ' + err.stack);
    return;
  }
  console.log('Connecté à MySQL en tant que ID : ' + connection.threadId);
});
