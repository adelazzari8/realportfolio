import { createClient } from '@supabase/supabase-js';

const urlBdd = import.meta.env.VITE_URL_BDD
const apiUrl = import.meta.env.VITE_URL_API_BDD

export const supabaseClient = createClient(urlBdd, apiUrl)

