import React from 'react';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import Portfolio from './components/pages/Portfolio.jsx';
import MainPage from './components/firstPage/MainPage.jsx';
import LoginPage from "./components/auth/LoginPage.jsx";
import RegisterPage from "./components/auth/RegisterPage.jsx";

function App() {
    return (
        <Routes>
            <Route path="/portfolio" element={<Portfolio/>}/>
            <Route path="/" element={<MainPage/>}/>
        </Routes>
    );
}

export default App;
