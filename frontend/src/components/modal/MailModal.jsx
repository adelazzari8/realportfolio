import React, { useEffect, useState } from 'react';
import { motion } from 'framer-motion';

const Modal = ({ isOpen, onClose, message,success }) => {
  const [timer, setTimer] = useState(5); // État pour le minuteur

  useEffect(() => {
    let interval;

    if (isOpen) {
      interval = setInterval(() => {
        if (timer > 0) {
          setTimer((prevTimer) => prevTimer - 1);
        } else {
          clearInterval(interval);
          onClose(); // Fermez la modal lorsque le minuteur atteint 0
        }
      }, 1000);
    }

    return () => clearInterval(interval);
  }, [isOpen, timer, onClose]);

  useEffect(() => {
    if (isOpen) {
      setTimer(5);
    }
  } , [onClose, isOpen]);

  if (!isOpen) return null;

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ position: 'fixed', top: '5rem', right: '1rem', zIndex: 100 }}
    >
        <div className="bg-black rounded-lg shadow-lg p-4 ">
            <button onClick={onClose} className="text-gray-400 hover:text-gray-500 transition ease-in-out duration-150 absolute top-0 right-0 mt-4 mr-4">
                <span className="sr-only">Close</span>
                <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none"
                viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                    d="M6 18L18 6M6 6l12 12" />
                </svg>
            </button>
            <div className="flex flex-row justify-between items-center mt-4">
                <h3 className={success ? "text-green-500 text-lg font-medium" : "text-red-500 text-lg font-medium"}>{message}</h3>
            </div>
            <p className="text-sm text-gray-500">Cette fenêtre se fermera automatiquement dans {timer} secondes.</p>
        </div>
    </motion.div>
    
  );
      
};

export default Modal;
