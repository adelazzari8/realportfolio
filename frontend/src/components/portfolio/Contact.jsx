import {useState, useRef, useEffect} from "react"
import {motion} from "framer-motion"
import emailjs from '@emailjs/browser'

import {styles} from '../../styles'
import {EarthCanvas} from "../canvas"
import {SectionWrapper} from "../../hoc"
import {slideIn} from "../../utils/motion"
import ReCAPTCHA from 'react-google-recaptcha';

import Modal from "../modal/MailModal"

const Contact = () => {
    const formRef = useRef()
    const [token, setToken] = useState('')
    const [form, setForm] = useState({
        name: "",
        email: "",
        message: "",
    })

    const [loading, setLoading] = useState(false)

    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [modalMessage, setModalMessage] = useState("");
    const [success, setSuccess] = useState(false);

    const handleChange = (e) => {
        const {target} = e;
        const {name, value} = target;

        setForm({
            ...form,
            [name]: value,
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault()
        setLoading(true)
        if (!form.name || !form.email || !form.message) {
            setLoading(false)
            setModalMessage("Veuillez remplir tous les champs correctement.");
            setSuccess(false);
            setModalIsOpen(true);
            return;
        }
        if (!token) {
            setLoading(false)
            setModalMessage("Veuillez cocher la case 'Je ne suis pas un robot'.");
            setSuccess(false);
            setModalIsOpen(true);
            return;
        }

        try {
            const response = await fetch(`${import.meta.env.VITE_API_URL}/addMail`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
            })
            const data = await response.json()
            if (response.status !== 200) {
                setModalMessage(data.message)
                setSuccess(false)
                setModalIsOpen(true)
                setLoading(false)
                return
            }

            let sendMail = await emailjs.send(
                import.meta.env.VITE_EMAILJS_SERVICE_ID,
                import.meta.env.VITE_EMAILJS_TEMPLATE_ID,
                {
                    name: form.name,
                    email: form.email,
                    message: form.message,
                    'g-recaptcha-response': token,
                },
                import.meta.env.VITE_EMAILJS_USER_ID
            )

            if (sendMail.status !== 200) {
                setModalMessage("Une erreur s'est produite")
                setSuccess(false)
                setModalIsOpen(true)
                setLoading(false)
                return
            }

            setModalMessage(data.message)
            setSuccess(true)
            setModalIsOpen(true)
            setLoading(false)
            setForm({
                name: "",
                email: "",
                message: "",
            })
        } catch (error) {
            setModalMessage("Une erreur s'est produite")
            setSuccess(false)
            setModalIsOpen(true)
            setLoading(false)
        }


    }


    return (
        <div className="xl:mt-12 xl:flex-row flex-col-reverse flex gap-10 overflow-hidden">
            <Modal
                isOpen={modalIsOpen}
                onClose={() => setModalIsOpen(false)}
                message={modalMessage}
                success={success}
            />
            <motion.div
                variants={slideIn("left", "tween", 0.2, 1)}
                className="flex-[0.75] bg-black-100 p-8 rounded-2xl"
            >
                <p className={styles.sectionSubText}>Me Contacter</p>
                <h3 className={styles.sectionHeadText}>Contact.</h3>

                    <form ref={formRef} onSubmit={handleSubmit} className="mt-12 flex flex-col gap-8">
                        <label className="flex flex-col">
                            <span className="text-white font-medium mb-4">Votre Nom</span>
                            <input
                                type="text"
                                name="name"
                                value={form.name}
                                onChange={handleChange}
                                placeholder="Quel est votre nom ?"
                                className="bg-tertiary py-4 px-6 placeholder:text-secondary text-white rounded-2xl focus:outline-none focus:ring-2 focus:ring-secondary "
                            />
                        </label>


                        <label className="flex flex-col">
                            <span className="text-white font-medium mb-4">Votre email</span>
                            <input
                                type="email"
                                name="email"
                                value={form.email}
                                onChange={handleChange}
                                placeholder="Quel est votre email ?"
                                className="bg-tertiary py-4 px-6 placeholder:text-secondary text-white rounded-2xl focus:outline-none focus:ring-2 focus:ring-secondary "
                            />
                        </label>


                        <label className="flex flex-col">
                            <span className="text-white font-medium mb-4">Votre Message</span>
                            <textarea
                                rows="7"
                                name="message"
                                value={form.message}
                                onChange={handleChange}
                                placeholder="Quel est votre message ?"
                                className="bg-tertiary py-4 px-6 placeholder:text-secondary text-white rounded-2xl focus:outline-none focus:ring-2 focus:ring-secondary "
                            />
                        </label>
                        <ReCAPTCHA
                            sitekey={import.meta.env.VITE_RECAPTCHA_SITE_KEY}
                            onChange={(token) => setToken(token)}
                            onExpired={() => setToken("")}
                            className="w-fit"
                        />


                        <button
                            type="submit"
                            className="bg-tertiary py-3 px-8 outline-non w-fit text-white font-bold shadow-md shadow-inner rounded-2xl hover:bg-secondary transition-all duration-300"
                        >
                            {loading ? "Envoi en cours..." : "Envoyer"}
                        </button>
                    </form>



            </motion.div>
            <motion.div
                variants={slideIn("right", "tween", 0.2, 1)}
                className="xl:flex-1 xl:h-auto md:h-[400px] h-[300px] "
            >
                <EarthCanvas/>
            </motion.div>
        </div>
    )
}

export default SectionWrapper(Contact, "contact")