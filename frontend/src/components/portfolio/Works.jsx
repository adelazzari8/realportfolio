
import { motion } from 'framer-motion'

import { styles } from '../../styles'
import { github } from '../../assets'
import { SectionWrapper } from '../../hoc'
import { projects } from '../../constants'
import { textVariant,fadeIn } from '../../utils/motion'
import { Tilt } from 'react-tilt'


const ProjectCard = ({ index,name,description,tags,image,source_code_link}) => {
  return (
    <motion.div variants={fadeIn('up','spring',index * 0.5,0.75)}>
      <Tilt
        option= {{
          max: 45,
          scale: 1,
          speed: 450,
        }}
        className="bg-tertiary p-5 rounded-2xl sm:w-[360px] w-full"
      >
        <div className="relative w-full h-[230px]">
          <img src={image} alt={name} className="w-full h-full object-cover rounded-2xl" />
        </div>
        <div className="absolute inset-0 flex justify-end m-3 card-img_hover">
          <div 
            onClick={() => window.open(source_code_link, "_blank")}
            className="flex justify-center items-center w-10 h-10 rounded-full black-gradient cursor-pointer"
          >
            <img src={github} alt="github" className="w-5 h-5 object-contain" />
          </div>
        </div>
        <div className="mt-5">
          <h3 className="text-secondary text-[20px] font-bold">{name}</h3>
          <p className="mt-2 text-secondary text-[15px]">{description}</p>
        </div>
        <div className="mt-4 flex flex-wrap gap-2">
          {tags.map((tag) => (
            <p key={tag.name} className={`${tag.color} text-[14px]`}>#{tag.name}</p>  
          ))}

        </div>
      </Tilt>
    </motion.div>
  )
}

const Works = () => {
  return (
    <>
      <motion.div
        variants={textVariant()}
        >
          <p className={styles.sectionSubText}>Mon travail</p>
          <h2 className={styles.sectionHeadText}>Mes Projets</h2>
      </motion.div>
      <div className="w-full flex">
        <motion.p
          variants={fadeIn("","",0.1,1)}
          className="mt-3 text-secondary text-[17px] max-w-3xl leading-[30px]"
        >
          Les projets suivants mettent en valeur mes compétences et l'expérience à travers
          des exemples concrets de mon travail. Chaque projet est brièvement décrit avec un
          liens vers les référentiels de code et un live démos dedans. Cela reflète ma
          capacité à résoudre des problèmes complexes, à travailler avec différentes 
          technologies, et gérer efficacement les projets.
        </motion.p>
      </div>
      <div className='mt-20 flex flex-wrap gap-7'> 
        {projects.map((project, index) => (
          <ProjectCard key={`project-${index}`} index={index} {...project} />
        ))}
      </div>
    </>
  )
}

export default SectionWrapper(Works, "projet")