import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

import { styles } from '../../styles'
import { navLinks } from "../../constants"
import { logo, menu, close } from '../../assets'

const Navbar = ({ activeSection }) => {
  const [active, setActive] = useState("");
  const [toggle, setToggle] = useState(false);

  useEffect(() => {
    setActive(activeSection);
  }, [activeSection]);

  return (
    <nav 
      className={`
      ${styles.paddingX}
      w-full flex 
      items-center
      py-5 fixed
      top-0 z-20
      bg-primary
      `}
    >
      <div className='w-full flex items-center justify-between items-center max-w-7xl mx-aud'>
        <Link 
          to='/'
          className="flex items-center gap-2"  
          onClick={
            () => {
              setActive("");
              window.scrollTo(0, 0);
            } 
          }
        >
          <img src={logo} alt="logo" className="w-9 h-9 object-contain rounded-full" />
          <p className='text-white font-bold text-[16px] cursor-pointer flex'>Alexis &nbsp;<span className='sm:block hidden'>| Portfolio</span></p>
        </Link>
        <ul className='list-none hidden md:flex flex-row gap-10 '>
          {
            navLinks.map((link) => (
              <li
                key={link.id}
                className={
                  `
                  ${
                    active === link.id ? "text-white" : "text-secondary"
                  } hover:text-white text-[18px] font-bold cursor-pointer transition-all duration-300
                  `
                }
                onClick={ () => { 
                  setActive(link.id);
                  }
                }
              >
                <a href={`#${link.id}`}>{link.title}</a>
              </li>
            ))
          }
          <Link to="/" 
            className={
              `
             hover:text-white text-[18px] font-bold cursor-pointer transition-all duration-300 ml-10
              `
            }
          >
          <li>Quitter</li>

          </Link>
        </ul>
        <div className='md:hidden flex flex-1 items-center justify-end'>
          <img 
            src={toggle ? close : menu } 
            alt="menu"
            className="w-[28px] h-[28px] object-contain cursor-pointer"
            onClick={
              () =>  setToggle(!toggle)
            }
          />

          <div className={`${!toggle ? 'hidden' : 'flex'} p-6 bg-gradient-to-r from-indigo-500 absolute top-20 right-0 mx-4 my-2 min-w-[140px] z-10 rounded-xl`}>
            <ul className='list-none flex justify-end items-start flex-col gap-4'>
            {
              navLinks.map((link) => (
                <li
                  key={link.id}
                  className={
                    `
                    ${
                      active === link.title ? "text-white" : "text-secondary"
                    } font-poppins font-medium text-[16px] cursor-pointer transition-all duration-300
                    `
                  }
                  onClick={ () => { 
                    setToggle(!toggle);
                    setActive(link.title);
                    }
                  }
                >
                  <a href={`#${link.id}`}>{link.title}</a>
                </li>
              ))
            }
            <Link to="/" 
            className={
              `
             hover:text-white text-[18px] font-bold cursor-pointer transition-all duration-300
              `
            }
          >
          <li>Quitter</li>

          </Link>
            </ul>
          </div>
        </div>

      </div>

    </nav>
  )
}

export default Navbar