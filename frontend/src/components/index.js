import { EarthCanvas, BallCanvas, ComputersCanvas, StarsCanvas } from './canvas';
import Hero from './portfolio/Hero';
import Navbar from './portfolio/Navbar';
import About from './portfolio/About';
import Experience from './portfolio/Experience';
import Works from './portfolio/Works';
import Contact from './portfolio/Contact';

export {
  Hero,
  Navbar,
  About,
  Experience,
  Works,
  Contact,
  EarthCanvas, 
  BallCanvas, 
  ComputersCanvas, 
  StarsCanvas
}