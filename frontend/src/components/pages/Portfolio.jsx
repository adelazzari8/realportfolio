import { About, Contact, Experience, Hero, Navbar, Works, StarsCanvas } from '..';
import { useEffect, useState } from 'react';
import { navLinks } from '../../constants';

const Portfolio = () => {
  const [visibleSectionId, setVisibleSectionId] = useState('');

  useEffect(() => {
    const handleScroll = () => {
      const sectionIds = navLinks.map((link) => link.id);
      
      for (const id of sectionIds) {
        const section = document.getElementById(id);
        if (section) {
          const rect = section.getBoundingClientRect();
          if (rect.top >= 0 && rect.bottom <= window.innerHeight) {
            setVisibleSectionId(id);
            break;
          }
        }
      }
    };

    window.addEventListener('scroll', handleScroll);
    handleScroll(); // Appel initial pour définir la section visible au chargement

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  return (

      <div className="relative z-0 bg-primary">
        <div className="bg-hero-pattern bg-cover bg-center bg-no-repeat"> 
          <Navbar activeSection={visibleSectionId} />
          <Hero />
        </div>
        <About />
        <Experience />
        <Works />
        <div className="relative z-0">
          <Contact />
          <StarsCanvas />
        </div>
      </div>

  );
}

export default Portfolio;