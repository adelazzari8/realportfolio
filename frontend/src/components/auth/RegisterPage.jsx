import React, { useState } from 'react';
import { motion } from 'framer-motion';
const RegisterPage = ({ isOpen, onClose, onRegisterSuccess,openLoginModal }) => {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [firstNameError, setFirstNameError] = useState(null);
    const [lastNameError, setLastNameError] = useState(null);
    const [emailError, setEmailError] = useState(null);
    const [passwordError, setPasswordError] = useState(null);

    const resetFields = () => {
        setFirstName('');
        setLastName('');
        setEmail('');
        setPassword('');
        setFirstNameError(null);
        setLastNameError(null);
        setEmailError(null);
        setPasswordError(null);
    };

    const handleFirstNameChange = (e) => {
        const newFirstName = e.target.value;
        setFirstName(newFirstName);

        // Vérifiez ici si le prénom est valide
        if (newFirstName.length < 2) {
            setFirstNameError("Le prénom doit contenir au moins 2 caractères.");
        } else {
            setFirstNameError(null); // Aucune erreur
        }
    };

    const handleLastNameChange = (e) => {
        const newLastName = e.target.value;
        setLastName(newLastName);

        // Vérifiez ici si le nom de famille est valide
        if (newLastName.length < 2) {
            setLastNameError("Le nom de famille doit contenir au moins 2 caractères.");
        } else {
            setLastNameError(null); // Aucune erreur
        }
    };

    const handleOnRegisterSuccess = (email) => {
        onRegisterSuccess(email);
        resetFields();
    }

    const handleEmailChange = (e) => {
        const newEmail = e.target.value;
        setEmail(newEmail);
        if (!/^.+@.+\..+$/.test(newEmail)) {
            setEmailError("L'adresse e-mail n'est pas valide.");
        } else {
            setEmailError(null); // Aucune erreur
        }
    };

    const handlePasswordChange = (e) => {
        const newPassword = e.target.value;
        setPassword(newPassword);
        let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;

        // Vérifiez ici si le mot de passe est valide
        if (!regex.test(newPassword)) {
            setPasswordError("Le mot de passe doit contenir au moins 8 caractères, une lettre majuscule, une lettre minuscule, un chiffre et un caractère spécial.");
        } else {
            setPasswordError(null); // Aucune erreur
        }
    };

    const handleCloseClick = () => {
        resetFields();
        onClose();
    };

    const handleOpenLoginModal = () => {
        resetFields();
        openLoginModal();
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await fetch(`${import.meta.env.VITE_API_URL}/register`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ firstName, lastName, email, password }),
            });
            if (response.ok) {
                handleOnRegisterSuccess(email);
            } else {
                const error = await response.json();
                if (error.message) {
                    setPasswordError(error.message);
                }
            }
        } catch (error) {
            setPasswordError("Une erreur s'est produite")
        }
    };

    return (
        <>
            {isOpen && (
                <div
                    style={{
                        position: 'fixed',
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: 0,
                        backgroundColor: 'rgba(0, 0, 0, 0.5)',
                        zIndex: 100,
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <motion.div
                        initial={{ opacity: 0, scale: 0.8 }}
                        animate={{ opacity: 1, scale: 1 }}
                        exit={{ opacity: 0, scale: 0.8 }}
                        style={{
                            maxWidth: '400px',
                            width: '100%',
                            backgroundColor: '#0c081a',
                            borderRadius: '8px',
                            padding: '20px',
                            zIndex: 101,
                            position: 'relative',
                            border: '1px solid #915eff'
                        }}
                    >
                        <button onClick={handleCloseClick} className="text-gray-400 hover:text-gray-500 transition ease-in-out duration-150 absolute top-4 right-4">
                            <span className="sr-only">Fermer</span>
                            <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 18L18 6M6 6l12 12" />
                            </svg>
                        </button>
                        <h3 className="text-white text-[24px] font-bold">S'inscrire</h3>
                        <form onSubmit={handleSubmit}>
                            <div className="mt-4">
                                <label htmlFor="firstName" className="block text-gray-300 text-sm">Prénom :</label>
                                <input type="text" id="firstName" value={firstName} onChange={handleFirstNameChange} className="w-full bg-tertiary text-white rounded-lg p-2 mt-1" />
                                {firstNameError && <p className="text-red-500 text-sm mt-1">{firstNameError}</p>}
                            </div>
                            <div className="mt-4">
                                <label htmlFor="lastName" className="block text-gray-300 text-sm">Nom de famille :</label>
                                <input type="text" id="lastName" value={lastName} onChange={handleLastNameChange} className="w-full bg-tertiary text-white rounded-lg p-2 mt-1" />
                                {lastNameError && <p className="text-red-500 text-sm mt-1">{lastNameError}</p>}
                            </div>
                            <div className="mt-4">
                                <label htmlFor="email" className="block text-gray-300 text-sm">Email :</label>
                                <input type="email" id="email" value={email} onChange={handleEmailChange} className="w-full bg-tertiary text-white rounded-lg p-2 mt-1" />
                                {emailError && <p className="text-red-500 text-sm mt-1">{emailError}</p>}
                            </div>
                            <div className="mt-4">
                                <label htmlFor="password" className="block text-gray-300 text-sm">Mot de passe :</label>
                                <input type="password" id="password" value={password} onChange={handlePasswordChange} className="w-full bg-tertiary text-white rounded-lg p-2 mt-1" />
                                {passwordError && <p className="text-red-500 text-sm mt-1">{passwordError}</p>}
                            </div>
                            <button type="submit" className="w-full bg-[#915eff] text-white rounded-lg p-2 mt-4 hover:bg-[#3f277d] hover:text-[#915eff] transition ease-in-out duration-150 mb-8">
                                S'inscrire
                            </button>
                        </form>
                        <button
                            onClick={() => handleOpenLoginModal()}
                            className='text-center text-white text-sm absolute bottom-4 right-4 hover:text-[#915eff] transition ease-in-out duration-150'
                        >
                            Connectez-vous
                        </button>
                    </motion.div>
                </div>
            )}
        </>
    );
}

export default RegisterPage;
