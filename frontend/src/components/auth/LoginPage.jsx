import {AnimatePresence, motion} from 'framer-motion';
import RegisterPage from "./RegisterPage.jsx";
import {useEffect, useState} from "react";
import {Subject} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

const api = import.meta.env.VITE_API_URL;

const LoginPage = ({isOpen, onClose, openRegisterModal, onLoginSuccess, registeredEmail  }) => {
        const [email, setEmail] = useState("");
        const [password, setPassword] = useState("");
        const [emailError, setEmailError] = useState("");
        const [passwordError, setPasswordError] = useState("");
        const [isButtonDisabled, setIsButtonDisabled] = useState(true);

        useEffect(() => {
                setEmail(registeredEmail);
                validateForm(registeredEmail, password);

        }, [registeredEmail]);


        const resetFields = () => {
            setEmail('');
            setPassword('');
            setEmailError(null);
            setPasswordError(null);
        };
        const handleEmailChange = (e) => {
            const newEmail = e.target.value;
            setEmail(newEmail);
            validateForm(newEmail, password);
        };

        const handlePasswordChange = (e) => {
            const newPassword = e.target.value;
            setPassword(newPassword);
            validateForm(email, newPassword);
        };

        const validateForm = (newEmail, newPassword) => {
            const isEmailValid = newEmail.trim() !== "" && isEmailSecure(newEmail);
            const isPasswordValid = newPassword.trim() !== "" && isPasswordSecure(newPassword);

            if (!isEmailValid) {
                setEmailError("Email non valide");
            } else {
                setEmailError("");
            }

            if (!isPasswordValid) {
                setPasswordError("Mot de passe non valide");
            } else {
                setPasswordError("");
            }

            setIsButtonDisabled(!(isEmailValid && isPasswordValid));
        };

        function isPasswordSecure(password) {
            const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
            return passwordRegex.test(password);
        }

        function isEmailSecure(email) {
            const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
            return emailRegex.test(email);
        }

        const handleCloseClick = () => {
            resetFields();
            onClose();
        };

        const handleLoginSuccess = (fullName) => {
            onLoginSuccess(fullName);
            resetFields();
            onClose();
        };

        const handleSubmit = async (e) => {
            e.preventDefault();
            try {
                const response = await fetch(`${import.meta.env.VITE_API_URL}/connexion`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        email,
                        password,
                    }),
                });

                if (response.ok) {
                    const data = await response.json();
                    localStorage.setItem('token', data.token);
                    handleLoginSuccess(data.fullName);
                } else {
                    const error = await response.json();
                    setEmailError(error.message);
                }
            } catch (error) {
                setEmailError("Une erreur s'est produite")
            }
        };

        useEffect(() => {
            const validationSubject = new Subject();

            const validationSubscription = validationSubject
                .pipe(
                    map(([emailValidation, passwordValidation]) => {
                        setEmailError(emailValidation);
                        setPasswordError(passwordValidation);
                    }),
                    catchError(() => {

                        return [];
                    })
                )
                .subscribe();

            return () => {
                validationSubscription.unsubscribe();
            };
        }, []);

        return (
            <>
                {isOpen && (
                    <div
                        style={{
                            position: 'fixed',
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0,
                            backgroundColor: 'rgba(0, 0, 0, 0.5)',
                            zIndex: 100,
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                    >
                        <motion.div
                            initial={{opacity: 0, scale: 0.8}}
                            animate={{opacity: 1, scale: 1}}
                            exit={{opacity: 0, scale: 0.8}}
                            style={{
                                maxWidth: '400px',
                                width: '100%',
                                backgroundColor: '#0c081a',
                                borderRadius: '8px',
                                padding: '20px',
                                zIndex: 101,
                                position: 'relative',
                                border: '1px solid #915eff'
                            }}
                        >
                            < button onClick={handleCloseClick}
                                     className="text-gray-400 hover:text-gray-500 transition ease-in-out duration-150 absolute top-4 right-4">
                                <span className="sr-only">Fermer</span>
                                <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none"
                                     viewBox="0 0 24 24"
                                     stroke="currentColor" aria-hidden="true">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                          d="M6 18L18 6M6 6l12 12"/>
                                </svg>
                            </button>
                            <h3 className="text-white text-[24px] font-bold">Se connecter</h3>
                            <form onSubmit={handleSubmit}>
                                <div className="mt-4">
                                    <label htmlFor="email" className="block text-gray-300 text-sm">Email :</label>
                                    <input
                                        type="email"
                                        id="email"
                                        className="w-full bg-tertiary text-white rounded-lg p-2 mt-1"
                                        value={email}
                                        onChange={handleEmailChange}
                                    />
                                </div>
                                <div className="mt-4">
                                    <label htmlFor="password" className="block text-gray-300 text-sm">Mot de passe
                                        :</label>
                                    <input
                                        type="password"
                                        id="password"
                                        className="w-full bg-tertiary text-white rounded-lg p-2 mt-1"
                                        value={password}
                                        onChange={handlePasswordChange}
                                    />
                                </div>
                                {emailError && <div className="text-red-500 text-sm mt-1">{emailError}</div>}
                                {passwordError && <div className="text-red-500 text-sm mt-1">{passwordError}</div>}
                                <button
                                    type="submit"
                                    className="w-full bg-[#915eff] text-white rounded-lg p-2 mt-4 hover:bg-[#3f277d] hover:text-[#915eff] transition ease-in-out duration-150 mb-8"
                                    disabled={isButtonDisabled}
                                >
                                    Se connecter
                                </button>
                            </form>
                            <button
                                onClick={() => openRegisterModal()}
                                className='text-center text-white text-sm absolute bottom-4 right-4 hover:text-[#915eff] transition ease-in-out duration-150'
                            >
                                Inscrivez-vous
                            </button>
                        </motion.div>
                    </div>
                )}
            </>
        );
    }
;

export default LoginPage;
