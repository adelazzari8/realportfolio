
import { useEffect, useState } from 'react';
import LoginPage from '../auth/LoginPage.jsx';
import RegisterPage from '../auth/RegisterPage.jsx';
import { iconAccount } from '../../constants';

const api = import.meta.env.VITE_API_URL;

const Header = () => {
    const [loginModalIsOpen, setLoginModalIsOpen] = useState(false);
    const [registerModalIsOpen, setRegisterModalIsOpen] = useState(false);
    const [userFullName, setUserFullName] = useState(null);
    const [registeredEmail, setRegisteredEmail] = useState('');

    useEffect(() => {
        async function fetchUser() {
            const token = localStorage.getItem('token');
            if (token) {
                const result = await fetch(`${import.meta.env.VITE_API_URL}/checkToken`, {
                    method: 'GET',
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });

                if (result.ok) {
                    const data = await result.json();
                    setUserFullName(data.fullName);
                    //on met dans un store redux le login et le nom de l'utilisateur pour le reutiliser dans la
                } else {
                    localStorage.removeItem('token');
                    setLoginModalIsOpen(true);
                }
            } else {
                setLoginModalIsOpen(true);
            }
        }
        fetchUser();
    }, []);




    const openRegisterModal = () => {
        setLoginModalIsOpen(false);
        setRegisterModalIsOpen(true);
    };

    const openLoginModal = () => {
        setLoginModalIsOpen(true);
        setRegisterModalIsOpen(false);
    };

    const closeLoginModal = () => {
        setLoginModalIsOpen(false);
    };

    const closeRegisterModal = () => {
        setRegisterModalIsOpen(false);
    };

    const handleRegisterSuccess = (email) => {
        setRegisteredEmail(email);
        openLoginModal();
    };

    const handleLogout = () => {
        localStorage.removeItem('token');
        setUserFullName(null);
    };

    return (
        <>
            <header>
                <div className="container mx-auto flex justify-end items-center">
                    <div className={`flex justify-end items-center ${!userFullName ? 'cursor-pointer' : ''}`} onClick={!userFullName && openLoginModal}>
                        <img src={iconAccount} alt="Image" style={{ width: '30px', height: '30px' }} />
                        {userFullName ? (
                            <>
                                <span className="text-white font-medium ml-4">{userFullName}</span>
                                <button className="ml-4 text-white font-medium hover:text-gray-400 transition ease-in-out duration-150" onClick={handleLogout}>
                                    Déconnexion
                                </button>
                            </>
                        ) : (
                            <span className="text-white font-medium ml-4">Connexion</span>
                        )}
                    </div>
                </div>
            </header>
            <LoginPage
                isOpen={loginModalIsOpen}
                registeredEmail={registeredEmail}
                onClose={closeLoginModal}
                openRegisterModal={openRegisterModal}
                onLoginSuccess={(fullName) => setUserFullName(fullName)}
            />
            <RegisterPage isOpen={registerModalIsOpen} onClose={closeRegisterModal} openLoginModal={openLoginModal} onRegisterSuccess={handleRegisterSuccess} />
        </>
    );
};

export default Header;
