import {motion} from 'framer-motion'
import {styles} from '../../styles'
import {Link} from 'react-router-dom';
import {SectionWrapper} from '../../hoc'
import {projects} from '../../constants'
import {textVariant, fadeIn} from '../../utils/motion'
import {Tilt} from 'react-tilt'
import Header from "../header/Header.jsx";


// eslint-disable-next-line react/prop-types
const ProjectCard = ({ index, name, description, tags, image, link, isLink }) => {
    const CardContent = (
        <motion.div variants={fadeIn('up', 'spring', index * 0.5, 0.75)}>
            <Tilt
                options={{
                    max: 45,
                    scale: 1,
                    speed: 450,
                }}
                className="bg-tertiary p-5 rounded-2xl sm:w-[360px] w-full"
            >
                <div className="relative w-full h-[230px]">
                    <img src={image} alt={name} className="w-full h-full object-cover rounded-2xl" />
                </div>
                <div className="mt-5">
                    <h3 className="text-secondary text-[20px] font-bold">{name}</h3>
                    <p className="mt-2 text-secondary text-[15px]">{description}</p>
                </div>
                <div className="mt-4 flex flex-wrap gap-2">
                    {tags.map((tag) => (
                        <p key={tag.name} className={`${tag.color} text-[14px]`}>#{tag.name}</p>
                    ))}
                </div>
            </Tilt>
        </motion.div>
    );

    return isLink ? (
        <Link to={link}>
            {CardContent}
        </Link>
    ) : (
        <a href={link} target="_blank" rel="noopener noreferrer">
            {CardContent}
        </a>
    );
};

const MainPage = () => {
    return (
        <>
            <Header/>
            <motion.div
                variants={textVariant()}
            >
                <p className={styles.sectionHeadText}>Bienvenue sur mon site</p>
                <p className={styles.sectionSubText}>Voici les différentes application disponible sur mon site</p>
                <p className='mt-3 text-secondary text-[17px] max-w-3xl leading-[30px]'>Cliquer sur l'application que
                    vous voulez utiliser !</p>
            </motion.div>
            <div className='mt-20 flex flex-wrap gap-7'>
                {projects.map((project, index) => project.link &&
                    <ProjectCard key={project.name} index={index} {...project} />)}
            </div>
        </>
    )
}

export default SectionWrapper(MainPage, "MainPage")