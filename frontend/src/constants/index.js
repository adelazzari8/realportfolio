import {
  mobile,
  backend,
  creator,
  web,
  javascript,
  typescript,
  html,
  css,
  reactjs,
  tailwind,
  nodejs,
  mongodb,
  git,
  figma,
  docker,
  chaussea,
  iut,
  carrent,
  threejs,
  portfolio,
  iconAccount,
  discordTwitch,
  lolManager,
} from "../assets";

export const navLinks = [
  {
    id: "about",
    title: "A propos",
  },
  {
    id: "work",
    title: "Parcours",
  },
  {
    id: "projet",
    title: "Projets",
  },
  {
    id: "contact",
    title: "Contact",
  },
  
];

export const about = {
  about_me: `Je suis un développeur Fullstack spécialisé principalement dans le backend avec Node.js 
  pour la création d'API en PHP. Je maîtrise également le développement front-end 
  en utilisant des frameworks tels que React. Je suis passionné par la création 
  d'applications web complètes, en mettant l'accent sur des fonctionnalités performantes 
  et une expérience utilisateur exceptionnelle. Travaillons ensemble pour concrétiser vos
  projets !`,
};

const services = [
  {
    title: "Developpeur Web",
    icon: web,
  },
  {
    title: "Developpeur React Native",
    icon: mobile,
  },
  {
    title: "Developpeur Backend",
    icon: backend,
  },
  {
    title: "Developpeur Fullstack",
    icon: creator,
  },
];

const technologies = [
  {
    name: "HTML 5",
    icon: html,
  },
  {
    name: "CSS 3",
    icon: css,
  },
  {
    name: "JavaScript",
    icon: javascript,
  },
  {
    name: "TypeScript",
    icon: typescript,
  },
  {
    name: "React JS",
    icon: reactjs,
  },
  {
    name: "Tailwind CSS",
    icon: tailwind,
  },
  {
    name: "Node JS",
    icon: nodejs,
  },
  {
    name: "MySQL",
    icon: mongodb,
  },
  {
    name: "Three JS",
    icon: threejs,
  },
  {
    name: "gitLab",
    icon: git,
  },
  {
    name: "figma",
    icon: figma,
  },
  {
    name: "docker",
    icon: docker,
  },
];

const experiences = [
  {
    title: "Bachelor universitaire de technologie",
    company_name: "IUT de METZ",
    icon: iut,
    iconBg: "#ffffff",
    date: "sept 2021 - juin 2023",
    points: [
       "Développement d'applications web et mobiles",
        "Développement d'applications en PHP et Node.js avec des bases de données MySQL",
        "Développement d'applications avec React Native",
        "Programmation orientée objet en TypeScript",
        "Développement d'api REST avec Node.js et Express ainsi que de documentation avec Swagger",
        "Création de base de données et de requêtes SQL qui respectent les formes normales",
    ],
  },
  {
    title: "Stage de fin d'études",
    company_name: "Chaussea",
    icon: chaussea,
    iconBg: "#E6DEDD",
    date: "avril 2023 - juin 2023",
    points: [
      "Restructuration de l'api de l'entreprise",
      "Collaboration avec les équipes de développement",
      "Optimisation des performances de l'api en respectant les bonnes pratiques",
      "Structuration du code en respectant les principes SOLID",
      "Création de documentation avec Swagger",
      "Utilisation de gitLab pour la gestion du code source",
    ],
  },
];

const testimonials = [
  {
    testimonial:
      "I thought it was impossible to make a website as beautiful as our product, but Rick proved me wrong.",
    name: "Sara Lee",
    designation: "CFO",
    company: "Acme Co",
    image: "https://randomuser.me/api/portraits/women/4.jpg",
  },
  {
    testimonial:
      "I've never met a web developer who truly cares about their clients' success like Rick does.",
    name: "Chris Brown",
    designation: "COO",
    company: "DEF Corp",
    image: "https://randomuser.me/api/portraits/men/5.jpg",
  },
  {
    testimonial:
      "After Rick optimized our website, our traffic increased by 50%. We can't thank them enough!",
    name: "Lisa Wang",
    designation: "CTO",
    company: "456 Enterprises",
    image: "https://randomuser.me/api/portraits/women/6.jpg",
  },
];

const projects = [
  {
    name: "Skuliliquizz",
    description:
      "Création d'un quizz en ligne pour les étudiants de l'IUT de Metz. Ainsi que la possibilité de jouer en ligne avec ses amis. via un système de salon.",
    tags: [
      {
        name: "angular",
        color: "blue-text-gradient",
      },
      {
        name: "mysql",
        color: "orange-text-gradient",
      },
      {
        name: "socket.io",
        color: "green-text-gradient",
      },
      {
        name: "tailwind",
        color: "pink-text-gradient",
      },
    ],
    image: carrent,
    source_code_link: "https://gitlab.com/adelazzari8/skuliliquizz",
  },
  {
    name: "Portfolio",
    description:
      "Création de mon portfolio en utilisant React et Tailwind CSS. Dans le but de montrer mes compétences en développement web.",
    tags: [
      {
        name: "react",
        color: "blue-text-gradient",
      },
      {
        name: "tailwind",
        color: "pink-text-gradient",
      },
    ],
    image: portfolio,
    source_code_link: "https://gitlab.com/adelazzari8/realportfolio",
    link: "/portfolio",
    isLink: true,
  },
  {
    name: "Bot TWITCH / DISCORD",
    description:
        "Création d'un bot pour un ami streamer sur Twitch. Ce bot permet de savoir si le streamer est en live ou non pour faire des annonces sur Discord.",
    tags: [
      {
        name: "TWITCH JS",
        color: "blue-text-gradient",
      },
      {
        name: "DISCORD JS",
        color: "pink-text-gradient",
      },
    ],
    image: discordTwitch  ,
    source_code_link: "https://github.com/Greenheros/Robot",
  },
  {
    name: "League of Legends",
    description:
        "Création d'un système complet pour le jeu League of Legends. Ce système permet de voir les champions, les items, les runes et les sorts, et de générer des compositions avec un système de commentaire de sélection et de gestion d'entités.",
    tags: [
      {
        name: "PHP",
        color: "blue-text-gradient",
      },
      {
        name: "SYMFONY",
        color: "pink-text-gradient",
      },
    ],
    image: lolManager,
    source_code_link: "https://github.com/AlexisDelazzari/evalPHP",
    link: "/evalphp/login",
    isLink: false
  },
];

export { services, technologies, experiences, testimonials, projects, iconAccount };
