const express = require('express');
const cors = require('cors');
require('dotenv').config();
const {countMail, addMail, deleteMail, updateDate} = require('./src/mail/requete');
const {connexion, register, checkLogin} = require('./src/auth/requete');
const jwt = require("jsonwebtoken");
const axios = require('axios');

const CryptoJS = require("crypto-js");
const nodemailer = require('nodemailer');


const app = express();

app.use(cors({origin: `${process.env.ORIGIN_URL}`}));

app.use(express.json());
app.listen(3001, () => {
    console.log("Serveur démarré (port 3001)");
});


app.post('/addMail', async (req, res) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        jwt.verify(token, process.env.TOKEN_SECRET, async (err, decodedToken) => {
            if (err) {
                res.status(403).json({message: 'Token invalide'});
            } else {
                await deleteMail();
                const result = await countMail(decodedToken.login);
                if (result < 3) {
                    await addMail(decodedToken.login);
                    await updateDate(decodedToken.login);
                    res.status(200).json({message: 'Mail envoyé avec succès'});
                } else {
                    res.status(403).json({message: 'Nombre de mail maximum atteint'});
                }
            }
        });
    } catch (error) {
        return res.status(403).json({message: 'Token invalide'});
    }
});

app.post('/connexion', async (req, res) => {
    const {email, password} = req.body;
    let hashedPassword = CryptoJS.SHA1(password);
    const result = await connexion(email, hashedPassword.toString());

    if (result) {
        const token = jwt.sign({login: email, fullName: result.fullName}, process.env.TOKEN_SECRET, {expiresIn: '1h'});
        res.status(200).json({message: 'Connexion réussie', token: token, fullName: result.fullName});
    } else {
        res.status(403).json({message: 'Mail ou mot de passe incorrect'});
    }
});

app.post('/register', async (req, res) => {
    const {firstName, lastName, email, password} = req.body;
    const result = await checkLogin(email);
    if (result) {
        res.status(403).json({message: 'Mail déjà utilisé'});
    } else {
        let hashedPassword = CryptoJS.SHA1(password);
        await register(email, hashedPassword.toString(), lastName, firstName);
        res.status(200).json({message: 'Inscription réussie'});
    }
});

app.get('/checkToken', async (req, res) => {
    const token = req.headers.authorization.split(' ')[1];
    if (token) {
        jwt.verify(token, process.env.TOKEN_SECRET, async (err, decodedToken) => {
            if (err) {
                res.status(403).json({message: 'Token invalide'});
            } else {
                const result = await checkLogin(decodedToken.login);
                if (result) {
                    res.status(200).json({
                        message: 'Token valide',
                        fullName: decodedToken.fullName,
                        login: decodedToken.login
                    });
                } else {
                    res.status(403).json({message: 'Token invalide'});
                }
            }
        });
    } else {
        res.status(403).json({message: 'Token invalide'});
    }
});

app.post("/verifyCaptcha", async (req, res) => {
    const {token} = req.body;

    const response = await axios.post(` https://www.google.com/recaptcha/api/siteverify`, {}, {
        params: {
            secret: process.env.CAPTCHA_SECRET_KEY,
            response: token
        }
    });

    console.log(response, 'response');
    if (response.data.success) {
        res.status(200).json({message: 'Captcha valide'});
    } else {
        res.status(403).json({message: 'Captcha invalide'});
    }
})
;

