const connection = require("../../connexion");

async function connexion(login, password) {
    try {
        const connexionSQL = `
            SELECT prenom AS fullName
            FROM auth
            WHERE login = ? AND password = ?
        `;

        return new Promise((resolve, reject) => {
            connection.query(connexionSQL, [login, password], function (err, result) {
                if (err) {
                    reject(err);
                } else {
                    resolve(result[0]);
                }
            });
        });
    } catch (error) {
        console.error("Error in connexion function:", error);
        throw error;
    }
}

async function checkLogin(login) {
    try {
        const checkLoginSQL = `
            SELECT login
            FROM auth
            WHERE login = ?
        `;

        return new Promise((resolve, reject) => {
            connection.query(checkLoginSQL, [login], function (err, result) {
                if (err) {
                    reject(err);
                } else {
                    resolve(result[0]);
                }
            });
        });
    } catch (error) {
        console.error("Error in checkLogin function:", error);
        throw error;
    }
}

async function register(login, password, nom, prenom) {
    try {
        const registerSQL = `
            INSERT INTO auth (login, password, nom, prenom, nbMail)
            VALUES (?, ?, ?, ?, 0)
        `;

        return new Promise((resolve, reject) => {
            connection.query(registerSQL, [login, password, nom, prenom], function (err, result) {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    } catch (error) {
        console.error("Error in register function:", error);
        throw error;
    }
}



module.exports = { connexion, checkLogin, register };