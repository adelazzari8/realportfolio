const connection = require("../../connexion");

async function deleteMail() {
    try {
        const deleteSQL = `
            UPDATE auth
            SET nbMail = 0
            WHERE created_at <= NOW() - INTERVAL 24 HOUR;
        `;

        return new Promise((resolve, reject) => {
            connection.query(deleteSQL, function (err, result) {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    } catch (error) {
        console.error("Error in deleteMail function:", error);
        throw error;
    }
}

async function updateDate(login) {
    try {
        const updateSQL = `
            UPDATE auth
            SET created_at = NOW()
            WHERE login = ?
            AND created_at <= NOW() - INTERVAL 24 HOUR;
        `;

        return new Promise((resolve, reject) => {
            connection.query(updateSQL, [login], function (err, result) {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    } catch (error) {
        console.error("Error in updateDate function:", error);
        throw error;
    }
}

async function countMail(login) {
    try {
        const countSQL = `
            SELECT nbMail AS count
            FROM auth
            WHERE login = ?
        `;

        return new Promise((resolve, reject) => {
            connection.query(countSQL, [login], function (err, result) {
                if (err) {
                    reject(err);
                } else {
                    resolve(result[0].count);
                }
            });
        });
    } catch (error) {
        console.error("Error in countMail function:", error);
        throw error;
    }
}

async function addMail(login) {
    try {
        const addSQL = `
            UPDATE auth
            SET nbMail = nbMail + 1
            WHERE login = ?
        `;

        return new Promise((resolve, reject) => {
            connection.query(addSQL, [login], function (err, result) {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    } catch (error) {
        console.error("Error in addMail function:", error);
        throw error;
    }
}



module.exports = { countMail, addMail, deleteMail,updateDate };
